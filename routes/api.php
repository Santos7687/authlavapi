<?php

// use Illuminate\Http\Request;

// use App\Http\Controllers\RoleController;

use App\Http\Controllers\Auth\UserController;
use App\Http\Controllers\Auth\RoleController;
use App\Http\Controllers\Auth\PermissionController;
use App\Http\Controllers\AuthController;
use App\Models\Auth\User;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;


// use App\Http\Controllers\Auth\RoleController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::prefix('auth')->group(function () {
//     Route::resource('/user',[UserController::class])->names('user');

// });
Route::post('/register',[AuthController::class,'register']);

Route::post('/login',[AuthController::class,'login']);
Route::group(['middleware'=>['auth:sanctum']],function(){
    Route::resource('/users', UserController::class)->names('user');
    Route::resource('/roles', RoleController::class)->names('role');
    Route::resource('/permissions', PermissionController::class)->names('permission');

});
// Route::middleware('auth:sanctum')->get('/users/{user}', function (Request $request) {
//     return $request->user();
// });

// Route::group(['prefix' => 'auth'], function () {
//     Route::resource('/users', UserController::class)->names('user');
//     Route::resource('/roles', RoleController::class)->names('role');
//     Route::resource('/permissions', PermissionController::class)->names('permission');

//     Route::group(['prefix' => 'v1'], function () {
//     });


    
//     // Route::get('/prueba',[RoleController::class,'prueba'] );
//     // Route::group()
//     // Route::resource('/role',[RoleController::class])->names('role');
//     // Route::resource('/user',[UserController::class])->names('user');
//     // Route::resource('/permission',[PermissionController::class])->names('permission');


// });

Route::get('/test',function(){
    $user=User::find(1);
    Gate::authorize('haveaccess','permission-40');
    return $user;
    // return $user->havePermission('permission-40');
});
 
