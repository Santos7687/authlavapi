<?php

namespace App\Models\Auth;

use Database\Factories\RoleFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;
    protected $fillable = ['name','slug','description','full_access'];


    public function roles()
    {
        return $this->belongsToMany(User::class, 'role_user',  'role_id', 'user_id')->withPivot('state','prueba')->withTimestamps();
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'permission_role',  'role_id', 'permission_id') ->withTimestamps();
    }
    protected static function newFactory()
    {
        return RoleFactory::new();
    }
}
