<?php

namespace App\Models\Traits;

use App\Models\Auth\Role;

trait UserTrait
{
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_user', 'user_id', 'role_id')->withPivot('state', 'prueba')->withTimestamps();
    }

    public function havePermission($permission)
    {
        foreach ($this->roles as $role) {
            if ($role['full_access'] && $role['state']) {
                return true;
            } else if ($role['state']){
                foreach ($role->permissions as $p) {
                    if ($p->slug == $permission && $p->state) {
                        return true;
                    }
                }
            }
        }
        return false;
        // return $this->roles;
    }
}
