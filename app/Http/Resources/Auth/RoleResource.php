<?php

namespace App\Http\Resources\Auth;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Http\Resources\Json\JsonResource;

class RoleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => 'roles',
            'attributes' => [
                'name' => $this->name,
                'description' => $this->description,
                'slug' => $this->slug,
                'full_access' => $this->full_access,
                'state'=>$this->state,
                // 'permisaio'=>$this->permissions,
                // 'pivot'=>$this->pivot
            ],
            'relationships' => [
                // 'pivot'=>$this->pivot,

                'permissions'   => PermissionResource::collection($this->permissions),
                // 'permissions2'   => PermissionResource::collection($this->permissions),
            ],
            'jsonapi' => [
                'version' => "1.0",
                'module'=>"authentication"
            ]
        ];
    }
    
}
