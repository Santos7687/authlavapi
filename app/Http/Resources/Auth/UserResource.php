<?php

namespace App\Http\Resources\Auth;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => 'users',
            'attributes' => [
                'name' => $this->name,
                'email' => $this->email,
                'state'=>$this->state,
                // 'pivot'=>$this->pívot
            ],
            'relationships' => [
                'roles'   => RoleResource::collection($this->roles),//llama si solo cargas en el with()
                // 'roles1'   => RoleResource::collection($this->roles),//llama cargues o no el en with()
            ],
            'jsonapi' => [
                'version' => "1.0",
                'module'=>"authentication"
            ]
        ];
        // return parent::toArray($request);
    }
}
