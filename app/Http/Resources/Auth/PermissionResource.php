<?php

namespace App\Http\Resources\Auth;

use Illuminate\Http\Resources\Json\JsonResource;

class PermissionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => 'permissions',
            'attributes' => [
                'name' => $this->name,
                'description' => $this->description,
                'state'=>$this->state
            ],
            // 'relationships' => [ ],
            'jsonapi' => [
                'version' => "1.0",
                'module'=>"authentication"
            ]
        ];
    }
}
