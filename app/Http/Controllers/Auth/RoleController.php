<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Resources\Auth\RoleResource;
use App\Models\Auth\Role;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Gate::authorize('haveaccess','role.index');
        $roles=Role::with(['permissions'])->orderBy('id','Desc')->paginate(5);
        // return $roles;
        return RoleResource::collection($roles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Gate::authorize('haveaccess','role.create');

        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Gate::authorize('haveaccess','role.create');

        try {
            $role = Role::create([
                'name' => $request['name'],
                'slug' => $request['slug'],
                'description' => $request['description'],
                'state' => '1',
            ]);
            return new RoleResource($role);
        } catch (Exception $e) {
            return response()->json(['errors' => $e, 'status' => false], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Gate::authorize('haveaccess','role.show');

        $role = Role::findOrFail($id);
        return new RoleResource($role);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Gate::authorize('haveaccess','role.update');

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Gate::authorize('haveaccess','role.update');

        try {
            $role = Role::findOrFail($id);
            // ->update([
            $role->name = $request['name'];
            $role->slug = $request['slug'];
            $role->description = $request['description'];
            $role->state = '1';
            // ]);
            // $role->update([
            //     'name' => $request['name'],
            //     'email' => $request['email'],
            //     'state' => '1',
            // ]);
            return new RoleResource($role);
        } catch (Exception $e) {
            return response()->json(['errors' => $e, 'status' => false], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Gate::authorize('haveaccess','role.delete');

        try {
            $role = Role::findOrFail($id);
            $role->state = $role->state ? '0' : '1';
            $role->save();
            return new RoleResource($role);
        } catch (Exception $e) {
            return response()->json(['errors' => $e, 'status' => false], 500);
        }
    }
}
