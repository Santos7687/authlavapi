<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Resources\Auth\UserResource;
use App\Models\Auth\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Gate::authorize('haveaccess','user.index');
        $users = User::orderBy('id', 'Desc')->with('roles')->paginate(5);
        return UserResource::collection($users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create',User::class);
        return 'create';
        // Gate::authorize('haveaccess','user.create');

        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Gate::authorize('haveaccess','user.create');

        // $validatedData = $request->validateWithBag([
        //     'email' => 'required|max:255|email|unique:users',
        //     'name' => 'required',
        // ]);
        $validatedData = $request->validate([
            'email' => 'required|unique:users|max:255',
        ]);
        try {
            $user = User::create([
                'name' => $request['name'],
                'email' => $request['email'],
                'state' => '1',
                'password' => Hash::make('12345678'),
            ]);
            return new UserResource($user);
        } catch (Exception $e) {
            return response()->json(['errors' => $e, 'status' => false], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        $this->authorize('view',[$user,['user.show','userown.show']]);

        return new UserResource($user);


        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $user = User::findOrFail($id);
        $this->authorize('update',[$user,['user.update','userown.update']]);
        // $this->authorize('update',$user);

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        Gate::authorize('haveaccess','user.update');

        try {
            $user=User::findOrFail($id);
            // ->update([
                $user->name = $request['name'];
                $user->email = $request['email'];
                $user->state = '1';
            // ]);
            // $user->update([
            //     'name' => $request['name'],
            //     'email' => $request['email'],
            //     'state' => '1',
            // ]);
            return new UserResource($user);
        } catch (Exception $e) {
            return response()->json(['errors' => $e, 'status' => false], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Gate::authorize('haveaccess','user.delete');

        try {
            $user = User::findOrFail($id);
            $user->state = $user->state ? '0' : '1';
            $user->save();
            return new UserResource($user);
        } catch (Exception $e) {
            return response()->json(['errors' => $e, 'status' => false], 500);
        }
    }
}
