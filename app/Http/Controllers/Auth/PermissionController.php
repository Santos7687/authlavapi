<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Resources\Auth\PermissionResource;
use App\Models\Auth\Permission;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Gate::authorize('haveaccess','permission.index');

        $permissions=Permission::orderBy('id','Desc')->paginate(5);
        // return $permissions;
        return PermissionResource::collection($permissions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Gate::authorize('haveaccess','permission.create');

        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Gate::authorize('haveaccess','permission.create');

        try {
            $permission = Permission::create([
                'name' => $request['name'],
                'slug' => $request['slug'],
                'description' => $request['description'],
                'state' => '1',
            ]);
            return new PermissionResource($permission);
        } catch (Exception $e) {
            return response()->json(['errors' => $e, 'status' => false], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Gate::authorize('haveaccess','permission.show');

        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Gate::authorize('haveaccess','permission.update');

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Gate::authorize('haveaccess','permission.update');

        try {
            $permission = Permission::findOrFail($id);
            // ->update([
            $permission->name = $request['name'];
            $permission->slug = $request['slug'];
            $permission->description = $request['description'];
            $permission->state = '1';
            // ]);
            // $permission->update([
            //     'name' => $request['name'],
            //     'email' => $request['email'],
            //     'state' => '1',
            // ]);
            return new PermissionResource($permission);
        } catch (Exception $e) {
            return response()->json(['errors' => $e, 'status' => false], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Gate::authorize('haveaccess','permission.delete');

        try {
            $permission = Permission::findOrFail($id);
            $permission->state = $permission->state ? '0' : '1';
            $permission->save();
            return new PermissionResource($permission);
        } catch (Exception $e) {
            return response()->json(['errors' => $e, 'status' => false], 500);
        }
    }
}
