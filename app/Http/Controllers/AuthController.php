<?php

namespace App\Http\Controllers;

use App\Models\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 400, 'message' => 'Bad Request']);
        }
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->save();

        return response()->json(['status' => 200, 'message' => 'User created successfully']);
    }
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 400, 'message' => 'Bad Request']);
        }

        $credentials = request(['email', 'password']);
        $credentials['state']=1;
        if (!Auth::attempt($credentials)) {
            return response()->json([
                'status' => 500,
                'message' => 'Unauthorized'
            ]);
        }

        $user=User::where('email',$request->email)->first();

        $tokenResult=$user->createToken('authToken')->plainTextToken;

        return response()->json([
            'status'=>200,
            'token'=>$tokenResult
        ]);

    }
}
