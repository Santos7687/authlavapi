<?php

namespace Database\Seeders;

use App\Models\Auth\Permission;
use App\Models\Auth\Role;
use App\Models\Auth\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */


    public function run()
    {
        $this->call(RoleSeeder::class);

        $user = User::factory(10)
            ->has(Role::factory()
                ->count(3)
                ->has(Permission::factory()
                    ->count(3)))
            ->create();



        // $user = User::factory()
        // ->hasRoles(1, [
        //     'name' => 'Editor'
        // ])
        // ->create();
        //     for($i=0;$i>10;$i++){
        //     // $permissions = Permission::count(3)->create([
        //     //     'name'=>'permiision'.$i
        //     // ]);

        //     }   
        //     // \App\Models\Auth\User::factory(10)->create();
        //     $permissions = Permission::factory(10)->create();
        //     // $roles = Role::factory(10)->count(3)->hasAttached($permissions)->create();
        //     // $user = User::factory(10)->count(3)->hasAttached($roles)->create();

    }
}
